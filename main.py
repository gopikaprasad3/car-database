import sqlite3
conn = sqlite3.connect('cars.db')
c = conn.cursor()

c.execute("""CREATE TABLE cars (
             car_name text ,
             owner_name text
           )""")

c.execute("INSERT INTO cars VALUES ('Rapid' ,'Tommy')")
c.execute("INSERT INTO cars VALUES ('Corolla' ,'Arthur')")
c.execute("INSERT INTO cars VALUES ('Wento' ,'John')")
c.execute("INSERT INTO cars VALUES ('Polo' ,'Grace')")
c.execute("INSERT INTO cars VALUES ('Creta' ,'Summer')")
c.execute("INSERT INTO cars VALUES ('i10' ,'Polly')")
c.execute("INSERT INTO cars VALUES ('Ecosport' ,'Theodore')")
c.execute("INSERT INTO cars VALUES ('Bravia' ,'Samantha')")
c.execute("INSERT INTO cars VALUES ('M G Hector' ,'Harsh')")
c.execute("INSERT INTO cars VALUES ('Honda City' ,'Eka')")

c.execute("SELECT * FROM cars" )
print(c.fetchall())
conn.commit()
conn.close()



